# AMBER_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([AMBER_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# AMBER_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([AMBER_SET_VERSION_INFO],
[
    amber_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    amber_major_version=`echo "$amber_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    amber_minor_version=`echo "$amber_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    amber_micro_version=`echo "$amber_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$amber_major_version"; then
        amber_major_version=0
    fi

    if test -z "$amber_minor_version"; then
        amber_minor_version=0
    fi

    if test -z "$amber_micro_version"; then
        amber_micro_version=0
    fi

    AMBER_VERSION="$amber_version"
    AMBER_MAJOR_VERSION=$amber_major_version
    AMBER_MINOR_VERSION=$amber_minor_version
    AMBER_MICRO_VERSION=$amber_micro_version

    if test -z "$4"; then
        AMBER_INTERFACE_AGE=0
    else
        AMBER_INTERFACE_AGE="$4"
    fi

    AMBER_BINARY_AGE=`expr 100 '*' $AMBER_MINOR_VERSION + $AMBER_MICRO_VERSION`
    AMBER_BINARY_VERSION=`expr 10000 '*' $AMBER_MAJOR_VERSION + \
                          $AMBER_BINARY_AGE`

    AC_SUBST(AMBER_VERSION)
    AC_SUBST(AMBER_MAJOR_VERSION)
    AC_SUBST(AMBER_MINOR_VERSION)
    AC_SUBST(AMBER_MICRO_VERSION)
    AC_SUBST(AMBER_INTERFACE_AGE)
    AC_SUBST(AMBER_BINARY_VERSION)
    AC_SUBST(AMBER_BINARY_AGE)

    AC_DEFINE_UNQUOTED(AMBER_MAJOR_VERSION, $AMBER_MAJOR_VERSION,
                       [AMBER major version number])
    AC_DEFINE_UNQUOTED(AMBER_MINOR_VERSION, $AMBER_MINOR_VERSION,
                       [AMBER minor version number])
    AC_DEFINE_UNQUOTED(AMBER_MICRO_VERSION, $AMBER_MICRO_VERSION,
                       [AMBER micro version number])
    AC_DEFINE_UNQUOTED(AMBER_INTERFACE_AGE, $AMBER_INTERFACE_AGE,
                       [AMBER interface age])
    AC_DEFINE_UNQUOTED(AMBER_BINARY_VERSION, $AMBER_BINARY_VERSION,
                       [AMBER binary version number])
    AC_DEFINE_UNQUOTED(AMBER_BINARY_AGE, $AMBER_BINARY_AGE,
                       [AMBER binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# AMBER_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([AMBER_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi    

    if test -z "$apidocdir"; then
        apidocdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/html'
    fi

    if test -z "$pipedocsdir"; then
         pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/'
    fi

    if test -z "$configdir"; then
       configdir='${datadir}/${PACKAGE}/config'
    fi

    if test -z "$scriptsdir"; then
       scriptsdir='${datadir}/esopipes/${PACKAGE}-${VERSION}/scripts'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(apidocdir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(configdir)
    AC_SUBST(scriptsdir)


    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(AMBER_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(AMBER_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# AMBER_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([AMBER_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    AMBER_INCLUDES='-I$(top_srcdir)/amber'
    AMBER_LDFLAGS='-L$(top_builddir)/amber'

    all_includes='$(AMBER_INCLUDES) $(CPL_INCLUDES) $(CX_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(AMBER_LDFLAGS) $(CPL_LDFLAGS) $(CX_LDFLAGS) $(EXTRA_LDFLAGS)'

    # Library aliases

    LIBAMBER='$(top_builddir)/amber/libamber.la'

    # Substitute the defined symbols

    AC_SUBST(AMBER_INCLUDES)
    AC_SUBST(AMBER_LDFLAGS)

    AC_SUBST(LIBAMBER)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(AMBER_INCLUDES) $(CPL_INCLUDES) $(CX_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(AMBER_LDFLAGS) $(CPL_LDFLAGS) $(CX_LDFLAGS)  $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])


# AMBER_SET_DID(SYMBOL, NAME)
#------------------------------
# Adds a DID identifier string to config.h
AC_DEFUN([AMBER_SET_DID],
[

    if test -n "$1" || test -n "$2"; then
        AC_DEFINE_UNQUOTED($1, "$2",
                  [Product DID identifier the library complies to])
        AC_SUBST(PRODUCT_DID)
    fi

])
