/*******************************************************************************
 * JMMC project ( http://www.jmmc.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
/**
 * @file
 * Miscellaneous useful functions.
 * 
 * This file contains functions:
 * - to handle program (main)
 */

/* 
 * System Headers
 */
#include <stdio.h>
#include <string.h>

/* 
 * Local Headers
 */
#include "amdlib.h"
#include "amdlibProtected.h"


/* 
 * Protected functions definition
 */

static amdlibUSERPREF_VALUES amdlibUserPrefValues[24]=
{
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0},
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0},
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0},
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0},
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0},
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0},
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0},
    {amdlibFALSE,0.0},{amdlibFALSE,0.0},{amdlibFALSE,0.0}
};

/**
 * Get the program name.
 *
 * Get the program name without its path, if argv0 was containing it.
 *
 * @param argv0 name of the program
 *
 * @return pointer on the program name.
 */
const char * amdlibGetProgramName(char * argv0)
{
    const char *endPath;
    const char *programName;
    
    /* Find if a path exists in front of program name */
    endPath = strrchr(argv0, '/');
    if (endPath != (char *)NULL)
    {
        /* A path exists: only retrieve program name which is 
         * after the last slash caracter 
         */
        programName = endPath + 1;
    }
    else
    {
        /* No path in front of program name */
        programName = argv0;
    }

    return programName;
}

amdlibUSERPREF_VALUES amdlibGetUserPref(int code)
{
    return(amdlibUserPrefValues[code]);
}

void amdlibSetUserPref(int code, amdlibDOUBLE value)
{
    amdlibUserPrefValues[code].set=amdlibTRUE;
    amdlibUserPrefValues[code].value=value;
}

void amdlibUnsetUserPref(int code)
{
    amdlibUserPrefValues[code].set=amdlibFALSE;
    amdlibUserPrefValues[code].value=0.0;
}



/*___oOo___*/
