/*******************************************************************************
 * JMMC project ( http://www.jmmc.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
/**
 * @file
 * Function to read data from IMAGING_DETECTOR binary table. 
 *
 * The IMAGING_DETECTOR binary table describe the setup and characteristics of
 * the 2-dimensional detector used to record photometric and visibility data.
 */

#define _POSIX_SOURCE 1

/* 
 * System Headers
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "fitsio.h"

/* 
 * Local Headers
 */
#include "amdlib.h"
#include "amdlibProtected.h"

/*
 * Protected function
 */

/**
 * Read data from header of the IMAGING_DETECTOR binary table and store 
 * it into the amdlibIMAGING_DETECTOR structure.
 * 
 * This function reads information from header of the IMAGING_DETECTOR binary 
 * table and store it in the given data structure. This concerns information 
 * related to :
 *    \li the origin of the data
 *    \li the instrument name
 *    \li the observation date
 *    \li the HDU creation date
 *    \li the Data dictionary name
 *    \li the DCS software version string
 *    \li the number of detectors
 *    \li the number of regions
 *    \li the maximum number of polynomial coefficients for DMC and DMP
 *    \li the number of dimensions of detector array
 *    \li the maximun number of telescopes for instrument
 *
 * Excepted for the 5 last fields (from the number of detectors to the maximun 
 * number of telescopes), if the informations is not found in header, the error
 * is ignored.

 * @param filePtr name of the FITS file containing IMAGING_DATA binary table.
 * @param imagingDetector structure where loaded information is stored.
 * @param errMsg error description message returned if function fails.
 *
 * @return
 * amdlibSUCCESS on successful completion. Otherwise amdlibFAILURE is returned.
 */
amdlibCOMPL_STAT amdlibReadImagingDetectorHdr
                                   (fitsfile               *filePtr, 
                                    amdlibIMAGING_DETECTOR *imagingDetector, 
                                    amdlibERROR_MSG        errMsg)

{
    int         status = 0;
    char        fitsioMsg[256];

    amdlibLogTrace("amdlibReadImagingDetectorHdr()"); 

    /* Reset error message */
    memset(errMsg, '\0', sizeof(amdlibERROR_MSG));

    /* Clear 'IMAGING_DETECTOR' data structure */
    memset(imagingDetector, '\0', sizeof(amdlibIMAGING_DETECTOR));
           
    /* Go to the IMAGING_DETECTOR binary table */
    if (fits_movnam_hdu(filePtr, BINARY_TBL, 
                        "IMAGING_DETECTOR", 0, &status) != 0)
    {
        amdlibReturnFitsError("IMAGING_DETECTOR");
    }

    /*** Read header informations */
    /* Read origine of the data and instrument name. If not found, the error
     * is ignored */
    if (fits_read_key(filePtr, TSTRING, "ORIGIN", &imagingDetector->origin,
                  NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "INSTRUME",
                      &imagingDetector->instrument, NULL, &status) != 0)
    {
        status = 0;
    }
    /* Read dates */
    if (fits_read_key(filePtr, TDOUBLE, "MJD-OBS", &imagingDetector->dateObsMJD,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "DATE-OBS", &imagingDetector->dateObs,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "DATE", &imagingDetector->date,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    /* Read dictionary names */
    if (fits_read_key(filePtr, TSTRING, "ESO DET DID", 
                      &imagingDetector->detDictionaryId, NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "ESO DET ID", &imagingDetector->detId,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    /* Read informations on data */
    if (fits_read_key(filePtr, TINT, "NDETECT", &imagingDetector->nbDetectors,
                      NULL, &status) != 0)
    {
        amdlibReturnFitsError("NDETECT");
    }     
    if (fits_read_key(filePtr, TINT, "NREGION", &imagingDetector->nbRegions,
                      NULL, &status) != 0)
    {
        amdlibReturnFitsError("NREGION");
    }   
    if (fits_read_key(filePtr, TINT, "NUM_DIM", &imagingDetector->numDim,
                      NULL, &status) != 0)
    {
        amdlibReturnFitsError("NUM_DIM");
    }   
    if (fits_read_key(filePtr, TINT, "MAX_COEF", &imagingDetector->maxCoef,
                      NULL, &status) != 0)
    {
        amdlibReturnFitsError("MAX_COEF");
    }   
    if (fits_read_key(filePtr, TINT, "MAXTEL", &imagingDetector->maxTel,
                      NULL, &status) != 0)
    {
        amdlibReturnFitsError("MAXTEL");
    }  

    return amdlibSUCCESS;
}

/*___oOo___*/
