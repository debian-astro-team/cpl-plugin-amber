/* $Id: gipaf.h,v 1.4 2011-09-26 12:54:06 agabasch Exp $
 *
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2004 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: agabasch $
 * $Date: 2011-09-26 12:54:06 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifndef GIPAF_H
#define GIPAF_H

#include <cxtypes.h>
#include <cpl.h>
/*
#include <cpl_macros.h>
#include <cpl_plist.h>
*/

#ifdef __cplusplus
extern "C" {
#endif


typedef struct _AmPaf_ AmPaf;

AmPaf *amber_paf_new(const cxchar *name, const cxchar *type,
                       const cxchar *id, const cxchar *description);
void amber_paf_delete(AmPaf *self);

cxchar *amber_paf_get_name(const AmPaf *self);
cxint amber_paf_set_name(AmPaf *self, const cxchar *name);

cxchar *amber_paf_get_type(const AmPaf *self);
cxint amber_paf_set_type(AmPaf *self, const cxchar *type);

cxchar *amber_paf_get_id(const AmPaf *self);
cxint amber_paf_set_id(AmPaf *self, const cxchar *id);

cxchar *amber_paf_get_description(const AmPaf *self);
cxint amber_paf_set_description(AmPaf *self, const cxchar *description);

cpl_propertylist *amber_paf_get_properties(const AmPaf *self);
cxint amber_paf_set_properties(AmPaf *self, const cpl_propertylist *properties);

cxint amber_paf_write(const AmPaf *self);

cxchar * amber_localtime_iso8601(void);

#ifdef __cplusplus
}
#endif

#endif /* GIPAF_H */
