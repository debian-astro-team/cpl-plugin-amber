/*
 * esolibSelector.h
 *
 *  Created on: Sep 25, 2009
 *      Author: agabasch
 */

#ifndef ESOLIBSELECTOR_H_
#define ESOLIBSELECTOR_H_
#include "cpl.h"
int amber_selector_lib(double x1, double x2, double x3, int ANDselection, int AverageFrames,
		const char * Method, const char * inname, const char * outname,
		int iIsScience, cpl_frameset * framelist, cpl_parameterlist * parlist,
		const char * recipename);

#endif /* ESOLIBSELECTOR_H_ */
