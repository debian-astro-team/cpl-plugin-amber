/*******************************************************************/
/* VLTI AMBER DATA REDUCTION SOFTWARE (AMDRS)                      */
/*                                                                 */
/* FILE: amdrs.h                                                   */
/*                                                                 */
/* Contains definitions of constant parameters, types and          */
/* prototypes for all ANSI C functions.                            */
/*                                                                 */
/* (tlicha 15-01-03) Initial Version                               */
/* handling routines                                               */
/*******************************************************************/ 

/******************************************************************************
*	Compiler directives
******************************************************************************/

/*
   Applicable documents:
   =====================

   [1]: LdV Data Processing algorithms
        VLT-SPE-ESO-15810-2112

   [2]: V. Coude du Foresto, S. Ridgway, J. M. Mariotti,
        'Deriving object visibilities from interferograms obtained
         with a fiber stellar interferometer'
        Astron. & Astrophys., Suppl. Ser. 121, pp. 379-392 (1997)

   we changed heavily the interfaces from the document [1], especially
   the main data has now its own data structure, and all data inside
   is in double, not in long, as described in [1];
 */



/* --------------------------------------------------------------- */
/* COMPILER DIRECTIVES                                             */
/* --------------------------------------------------------------- */
#ifndef _AMDRS_H
#define _AMDRS_H 

/*
#define SECURITY_MODE
*/

/* Compiler directive. If it is set, the program will run slower 
   but more safe (dimension checks etc. are made.) Keep this 
   flag set as long as you developing the code. */

/* --------------------------------------------------------------- */
/* INCLUDES                                                        */
/* --------------------------------------------------------------- */

#include <math.h>
#include <time.h>
/*#include "fftw.h" */    /* for FFTW library */

//#include "amberSupport.h" 

#include "fitsio.h" /* needed by amdlib.h */

#include "amdlib.h"
#include "amdlibProtected.h"




/* --------------------------------------------------------------- */
/* Set CPL Version 3.0 compliance                                  */
/* uncomment this line to be CPL 2.1 compatible.                   */
/* --------------------------------------------------------------- */
/* #define AMBER_CPL_2_1 */

/* --------------------------------------------------------------- */
/* DEBUG OUTPUT ON = 1                                             */
/* --------------------------------------------------------------- */
#define TL_DEBUG 1

/* --------------------------------------------------------------- */
/* AMBER INSTRUMENT                                                */
/* --------------------------------------------------------------- */
#define AMBER_SUBWINDOW_X_MAX    16  /* width of a subwindow */
#define AMBER_SUBWINDOW_Y_MAX    32  /* height of a subwindow */

/* --------------------------------------------------------------- */
/* MATHEMATICAL CONSTANTS                                          */
/* --------------------------------------------------------------- */
/*#define PI 3.141592653589793238e+00*/
/* value taken from MATHEMATICA, (c) Wolfram Research 
   Why this high precision?
   Example: Phase k*z with k=2*pi/lambda, lambda = 1e-06 m and z = 1km
   We should get a Phase of zero, as a multiple of 1e-06 fits exactly inside 1km. 
   (1) if we take pi = 3.141592654 --> Phase = 6.283185308000000e+09
              --> (cos(Phase),sin(Phase)) = (6.819186241461076e-01, 7.314280484385868e-01)
   (2) if we take pi = 3.141592653589793 --> Phase = 6.283185307179587e+09
              --> (cos(Phase),sin(Phase)) = (9.999999999993591e-01, 1.132200850406420e-06)
   The correct (cos,sin) pair would be (1,0) --> (2) is much better than (1) */
#define TWO_PI 6.283185307179586476e+00

/* --------------------------------------------------------------- */
/* AMBER DRS CONSTANTS                                             */
/* --------------------------------------------------------------- */



/* --------------------------------------------------------------- */
/* OTHER CONSTANTS ...                                             */
/* --------------------------------------------------------------- */
#define MEMORY_PROTOCOL_THRESHOLD 1000
/* when writing an ASCII memory protocol, only increments whose absolute
   value is larger than this threshold (in units of BYTES) are written
   to the protocol */

#define MAX_STRING_LENGTH 255
/* the maximum length of a string used in the code */


 
typedef struct 
{ 
  double time; 
  double dl_offset;  
  double mu1_squared;  
  double mu2_squared;  
} scan_data; 
/* A 'scan_data' stores the informations related to 1 scan */ 

 
 
/* Memory struct (contains all information on current memory usage) */ 
/* ---------------------------------------------------------------- */ 
typedef struct 
{ 
    long currently_allocated; 
    /* currently used memory is stored in the global variable 'vndrs_memory.currently allocated' */ 
 
    long delta; 
    /* when writing an ASCII memory usage protocol, not every allocation or free process 
       is written to the protocol. Changes in total memory are added up in the 
       variable "vndrs_memory.delta". Only if this value is larger than the treshold 
       (defined above as a constant) the protocol is updated. */ 
 
    FILE *file_pointer; 
    /* file pointer to the memory protocol file */ 
 
    /* COUNTERS THAT KEEP TRACK OF HOW MANY TIMES SPECIFIC ALLOCATION AND 
       FREE ROUTINES HAVE BEEN CALLED */ 
    long calloc_vector_double_calls; 
    long calloc_vector_complex_calls; 
    long calloc_matrix_double_calls; 
    long calloc_matrix_complex_calls; 
    long calloc_full_scan_calls; 
    long calloc_full_pow_calls; 
    long calloc_batch_calls; 
    long calloc_powbatch_calls; 
} memory_usage; 
 
/* Protocol struct (contains information on the runtime protocol) */ 
/* -------------------------------------------------------------- */ 
typedef struct 
{ 
    char mode[MAX_STRING_LENGTH]; 
    /* can be either 'NONE', 'SCREEN' or 'DISK' */ 
 
    FILE *file_pointer; 
    /* file pointer to the runtime protocol */ 
 
    char text[MAX_STRING_LENGTH]; 
    /* the string that shall be written to the runtime protocol */  
} protocol; 
 
 
/* --------------------------------------------------------------- */ 
/* GLOBAL VARIABLES                                                */ 
/* --------------------------------------------------------------- */ 
 
/* time_t StartTime; */  
/* time_t EndTime;   */

/* time at start and end of program execution */ 
 
/* memory_usage amdrs_memory; */
/* struct that keeps track of the memory usage */ 
 
/* protocol amdrs_protocol; */
/* struct responsible for the runtime protocol */ 
 
/* char   DataPath[MAX_STRING_LENGTH]; */
/* path used to store intermediate data */ 
 
 
/* &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */ 
/* &&                                                           && */ 
/* &&         FUNCTION PROTOTYPES (REQUIRED IN ANSI C)          && */ 
/* &&                                                           && */ 
/* &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */ 
 
/* ------------------------------------------------------------------- */ 
/* PROGRAM FLOW / RUNTIME PROTOCOL                                     */ 
/* ------------------------------------------------------------------- */ 
/*void vndrs_initialization(char *protocol_mode, char *file_name); */
/*void vndrs_exit(void); */
/*void vndrs_start_runtime_protocol(char *protocol_mode, char *file_name); */
/*void vndrs_runtime_protocol(void); */
/*void vndrs_end_runtime_protocol(void);*/ 
void amdrs_error_alert(void);  
 

/* --------------------------------------------------------------- */ 
/* AMBER DRS INCLUDES                                              */ 
/* --------------------------------------------------------------- */ 
 
/*#include "vndrs_IO.h"*/ 

 
#endif
