/*
 * esolibTransferfunction.h
 *
 *  Created on: Oct 5, 2009
 *      Author: agabasch
 */

#ifndef ESOLIBTRANSFERFUNCTION_H_
#define ESOLIBTRANSFERFUNCTION_H_

#define DEG_TO_RAD              (0.017453292519943295474372)
#define HOUR_TO_RAD             (0.261799387799149407829447)
/* Minimum threshold for finding diameter in radians (60 arcsec) */
#define CALIBRATOR_MIN_THRESHOLD (0.000290888)
#define RAD_TO_ARCSEC           (206264.8062470963551564733)
#define AMBER_PI                 (3.141592653589793115997963)

/* Inverse Effective wavelength = (1.0 / 10 micron) from midi*/
 /*needed only for crosscheck with MIDI results !!!*/
#define INVERSE_WAVELENGTH       (100000.0)
/* Diameter factor = (1.0 / (1000 * 206264.8)) */
#define DIAMETER_FACTOR          (4.848136958e-9)
#include "cpl.h"
typedef struct
{
	double       calibRA;
	double       calibDEC;
	double       calibDistance;
	const char * calibNameDatabase;
	const char * calibNameObserved;
	cpl_vector * calibInvWavelength;
	cpl_vector * calibVis;
	cpl_vector * calibVisErr;
	cpl_vector * calibVisSqrd;
	cpl_vector * calibVisSqrdErr;
	double       calibPblAverage;
	double       calibParangAverage;
	double       calibDiameter;
	double       calibDiameterErr;
	double       calibMagJ;
	double       calibMagH;
	double       calibMagK;
	int          calibFlag;
	int          calibEsoTrusted;
	float mcalibVis;         /*only for crosscheck with MIDI results*/
	float mcalibVisErr;      /*only for crosscheck with MIDI results*/
	float mcalibVisSqrd;     /*only for crosscheck with MIDI results*/
	float mcalibVisSqrdErr;  /*only for crosscheck with MIDI results*/
} CalibratorParam;

cpl_error_code amber_TransferFunction(const char * recipename,
		const char * outname_calibrator, cpl_parameterlist *parlist,
		cpl_frameset * framelist);

#endif /* ESOLIBTRANSFERFUNCTION_H_ */
