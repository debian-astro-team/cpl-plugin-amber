/*
 * amber_dfs.c
 *
 *  Created on: Oct 13, 2009
 *      Author: agabasch
 */
#include "amber_dfs.h"
cpl_error_code amber_dfs_set_groups(cpl_frameset * set)
{
	cpl_errorstate prestate = cpl_errorstate_get();
	cpl_frame * frame = NULL;
	int         i = 0;
    int             nframes ;

    /* Initialize */
    nframes = cpl_frameset_get_size(set) ;

    /* Loop on frames */
    for (i=0 ; i<nframes ; i++) {
        frame = cpl_frameset_get_position(set, i) ;
		const char * tag = cpl_frame_get_tag(frame);
		if (tag == NULL) {
			cpl_msg_warning(cpl_func, "Frame %d has no tag", i);
		} else if (!strcmp(tag,"AMBER_2P2V"        ) ||
				!strcmp(tag,"AMBER_2WAVE"          ) ||
				!strcmp(tag,"AMBER_3P2V"           ) ||
				!strcmp(tag,"AMBER_3WAVE"          ) ||
				!strcmp(tag,"AMBER_DETECTOR_DARK"  ) ||
				!strcmp(tag,"AMBER_DETECTOR_FFM"   ) ||
				!strcmp(tag,"AMBER_SKY"            ) ||
				!strcmp(tag,"AMBER_CALIB"          ) ||
				!strcmp(tag,"AMBER_COHER"          ) ||
				!strcmp(tag,"AMBER_BEAMPOS"        ) ||
				!strcmp(tag,"AMBER_DARK"           ) ||
				!strcmp(tag,"AMBER_COLPOS"         ) ||
				!strcmp(tag,"AMBER_DARK_CALIB"     ) ||
				!strcmp(tag,"AMBER_DARK_SCIENCE"   ) ||
				!strcmp(tag,"AMBER_FLUX"           ) ||
				!strcmp(tag,"AMBER_P2VM"           ) ||
				!strcmp(tag,"AMBER_SCIENCE"        ) ||
				!strcmp(tag,"AMBER_SCIENCE_CALIB"  ) ||
				!strcmp(tag,"AMBER_SEARCH"         ) ||
				!strcmp(tag,"AMBER_SKY_CALIB"      ) ||
				!strcmp(tag,"AMBER_SKY_SCIENCE"    ))
		{
			/* RAW frames */
			cpl_frame_set_group(frame, CPL_FRAME_GROUP_RAW   );
		} else if (!strcmp(tag, "AMBER_BADPIX"         ) ||
				!strcmp(tag, "AMBER_FLATFIELD"         ) ||
				!strcmp(tag, "P2VM_REDUCED"            ) ||
				!strcmp(tag, "AMBER_BEAMPOS_REDUCED"   ) ||
				!strcmp(tag, "CALIB_REDUCED"           ) ||
				!strcmp(tag, "CALIB_REDUCED_FILTERED"  ) ||
				!strcmp(tag, "SCIENCE_REDUCED"         ) ||
				!strcmp(tag, "SCIENCE_REDUCED_FILTERED") ||
				!strcmp(tag, "BADPIX_REDUCED"          ) ||
				!strcmp(tag, "FLATFIELD_REDUCED"       ) ||
				!strcmp(tag, "AMBER_TRF_J"             ) ||
				!strcmp(tag, "AMBER_TRF_H"             ) ||
				!strcmp(tag, "AMBER_TRF_K"             ) ||
				!strcmp(tag, "AMBER_CALIB_DATABASE_J"  ) ||
				!strcmp(tag, "AMBER_CALIB_DATABASE_H"  ) ||
				!strcmp(tag, "AMBER_CALIB_DATABASE_K"  ) ||
				!strcmp(tag, "CALIB_DATABASE_J"        ) ||
				!strcmp(tag, "CALIB_DATABASE_H"        ) ||
				!strcmp(tag, "CALIB_DATABASE_K"        ) ||
				!strcmp(tag, "SCIENCE_CALIBRATED"  ))
		{
			/* CALIB frames */
			cpl_frame_set_group(frame, CPL_FRAME_GROUP_CALIB);
		}







	}

	if (!cpl_errorstate_is_equal(prestate)) {
		return cpl_error_set_message(cpl_func, cpl_error_get_code(),
				"Could not identify RAW and CALIB "
				"frames");
	}

	return CPL_ERROR_NONE;
}

/**
 * @brief
 *   Append the JMMC acknowledgement to a property list.
 *
 * @param self   A property list.
 *
 * @return
 *   The function returns @c CPL_ERROR_NONE on success or a CPL error
 *   code otherwise.
 *
 * The property is appended to the property list @em self.
 */
cpl_error_code  amber_JMMC_acknowledgement(cpl_propertylist * plist){

	cpl_errorstate prestate = cpl_errorstate_get();

	//cpl_propertylist_append_string(plist, "COMMENT", " -- ");
	cpl_propertylist_append_string(plist, "COMMENT", "If you use the AMBER data reduction software, JMMC asks you to");
	cpl_propertylist_append_string(plist, "COMMENT", "add this sentence in the acknowledgement section of your");
	cpl_propertylist_append_string(plist, "COMMENT", "articles:");
	//cpl_propertylist_append_string(plist, "COMMENT", " -- ");
	cpl_propertylist_append_string(plist, "COMMENT", "This research has made use of the \\texttt{AMBER data");
	cpl_propertylist_append_string(plist, "COMMENT", "reduction package} of the Jean-Marie Mariotti");
	cpl_propertylist_append_string(plist, "COMMENT", "Center\\footnote{Available at http://www.jmmc.fr/amberdrs}.");
	//cpl_propertylist_append_string(plist, "COMMENT", " -- ");
	cpl_propertylist_append_string(plist, "COMMENT", "Furthermore, as user of the AMBER data reduction software, you");
	cpl_propertylist_append_string(plist, "COMMENT", "are kindly requested to cite the two following refereed papers");
	cpl_propertylist_append_string(plist, "COMMENT", "in the data reduction section of your article(s):");
	//cpl_propertylist_append_string(plist, "COMMENT", " -- ");
	cpl_propertylist_append_string(plist, "COMMENT", "Tatulli et al. (2007, available at");
	cpl_propertylist_append_string(plist, "COMMENT", "http://cdsads.u-strasbg.fr/abs/2007A&A...464...29T)");
	cpl_propertylist_append_string(plist, "COMMENT", "and Chelli et al. (2009, available at");
	cpl_propertylist_append_string(plist, "COMMENT", "http://cdsads.u-strasbg.fr/abs/2009A&A...502..705C).");
	//cpl_propertylist_append_string(plist, "COMMENT", " -- ");


	if (!cpl_errorstate_is_equal(prestate)) {
		return cpl_error_set_message(cpl_func, cpl_error_get_code(),
				"Could not write JMMS acknowledgement ");
	}
	return CPL_ERROR_NONE;
}

