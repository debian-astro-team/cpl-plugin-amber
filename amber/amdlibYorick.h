/*******************************************************************************
 * JMMC project ( http://www.jmmc.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
#ifndef amdlib_H
#define amdlib_H

/**
 * @file
 * This file contains declaration of functions which have been developped for
 * Yorick interfacing 
 */

/* The following piece of code alternates the linkage type to C for all
 * functions declared within the braces, which is necessary to use the
 * functions in C++-code.
 */

#ifdef __cplusplus
extern "C" { 
#endif

amdlibCOMPL_STAT amdlibCopyRegionInfo (/* Input */
                                       amdlibREGION    *srcRegions, 
                                       /* Output */
                                       amdlibREGION    *dstRegions, 
                                       int             nbRegions);

amdlibCOMPL_STAT amdlibCopyRegionData (/* Input */
                                       amdlibREGION    *srcRegions, 
                                       /* Output */
                                       amdlibREGION    *dstRegions, 
                                       int             nbRegions,
                                       amdlibERROR_MSG errMsg);

amdlibCOMPL_STAT amdlibCopyScienceData (/* Input */
                                        amdlibSCIENCE_DATA *srcScienceData, 
                                        /* Output */
                                        amdlibSCIENCE_DATA *dstScienceData, 
                                        amdlibERROR_MSG    errMsg);

amdlibCOMPL_STAT amdlibCopySelection (/* Input */
                                      amdlibSELECTION *srcSelection,
                                      /* Output */
                                      amdlibSELECTION *dstSelection);
                                      
amdlibCOMPL_STAT amdlibCopyOiArray(amdlibOI_ARRAY  *srcOiArray,
                                   amdlibOI_ARRAY  *dstOiArray);

amdlibCOMPL_STAT amdlibCopyOiTarget(amdlibOI_TARGET *srcOiTarget,
                                    amdlibOI_TARGET *dstOiTarget);

amdlibCOMPL_STAT amdlibCopyPhotometry (/* Input */
                                       amdlibPHOTOMETRY *srcPhotometry, 
                                       /* Output */
                                       amdlibPHOTOMETRY *dstPhotometry);

amdlibCOMPL_STAT amdlibCopyVis(/* Input */
                               amdlibVIS       *srcVis, 
                               /* Output */
                               amdlibVIS       *dstVis);

amdlibCOMPL_STAT amdlibCopyVis2(/* Input */
                                amdlibVIS2      *srcVis2, 
                                /* Output */
                                amdlibVIS2      *dstVis2);

amdlibCOMPL_STAT amdlibCopyVis3(/* Input */
                                amdlibVIS3      *srcVis3, 
                                /* Output */
                                amdlibVIS3      *dstVis3);

amdlibCOMPL_STAT amdlibCopyWavelength (/* Input */
                                       amdlibWAVELENGTH *srcWlen, 
                                       /* Output */
                                       amdlibWAVELENGTH *dstWlen);

amdlibCOMPL_STAT amdlibCopyPiston(/* Input */
                                  amdlibPISTON    *srcOpd, 
                                  /* Output */
                                  amdlibPISTON    *dstOpd);
amdlibCOMPL_STAT amdlibCopySpectrum(amdlibSPECTRUM  *srcSpectrum, 
                                    amdlibSPECTRUM  *dstSpectrum);

amdlibCOMPL_STAT amdlibCopyBadPixelMap(amdlibBAD_PIXEL_MAP *dstBadPixel);

amdlibCOMPL_STAT amdlibCopyFlatFieldMap(amdlibFLAT_FIELD_MAP *dstFlatField);

#ifdef __cplusplus
}
#endif


#endif /*!amdlib_H*/
