/*
 * esolibCalibVis.h
 *
 *  Created on: Oct 26, 2009
 *      Author: agabasch
 */

#ifndef ESOLIBCALIBVIS_H_
#define ESOLIBCALIBVIS_H_

#include "cpl.h"

/*-----------------------------------------------------------------------------
                           Missing Function Prototypes
 -----------------------------------------------------------------------------*/
#if defined HAVE_ISNAN && HAVE_ISNAN != 0
#if !defined isnan && defined HAVE_DECL_ISNAN && HAVE_DECL_ISNAN == 0
/* HP-UX and Solaris may have isnan() available at link-time
   without the prototype */
int isnan(double);
#endif
#endif


int amber_isnan(double value);


cpl_error_code amber_CalibVis(const char * recipename,
		const char * outname, cpl_parameterlist *parlist,
		cpl_frameset * frameset);


#endif /* ESOLIBCALIBVIS_H_ */
