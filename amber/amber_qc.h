#ifndef AMBER_QC_H
#define AMBER_QC_H



#define USE_CEN  0.80
#define USE_BIN1 0.25
#define USE_BIN2 0.25
#define USE_BIN3 0.25

#ifndef M_PI
#define M_PI            3.14159265358979323846  /* pi */
#endif /* M_PI */

#include "amdlib.h"
#include "amdlibProtected.h"
#include <cpl.h>

int amber_qc(amdlibWAVELENGTH *imdWave, amdlibVIS *imdVis, amdlibVIS2 *imdVis2,
		amdlibVIS3 *imdVis3, amdlibSPECTRUM *spectrum, cpl_propertylist * qclist, const char * calib);

#endif
