/*
 * amber_dfs.h
 *
 *  Created on: Oct 13, 2009
 *      Author: agabasch
 */

#ifndef AMBER_DFS_H_
#define AMBER_DFS_H_

#include <cpl.h>
#include <string.h>
cpl_error_code amber_dfs_set_groups(cpl_frameset *);
cpl_error_code  amber_JMMC_acknowledgement(cpl_propertylist * plist);
#endif /* AMBER_DFS_H_ */
