/*******************************************************************************
 * JMMC project ( http://www.jmmc.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
/**
 * @file
 * Function to handle the header of imaging data binary table. 
 *
 * The IMAGING_DATA binary table gives the detector pixel data for an exposure.
 */

#define _POSIX_SOURCE 1

/* 
 * System Headers
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "fitsio.h"

/* 
 * Local Headers
 */
#include "amdlib.h"
#include "amdlibProtected.h"

/* 
 * Protected function 
 */
/**
 * Read information from header of the IMAGING_DATA binary table and store 
 * it into the amdlibIMAGING_DATA structure.
 *
 * This function reads information from header of the IMAGING_DATA binary table
 * and store it in the given data structure. This concerns information related
 * to :
 *      \li the origin of the data
 *      \li the instrument name
 *      \li the observation date
 *      \li the HDU creation date
 *      \li the Data dictionary name
 *      \li the DCS software version string
 *      \li the number of regions
 *      \li the maximun number of telescopes for instrument
 *      \li the maximum number of entries in OPTICAL_TRAIN table
 *      \li the maximum number of steps in an observational cycle
 *
 * Excepted for the number of regions, if information is not found in header, 
 * the error is ignored.
 *
 * @param filePtr name of the FITS file containing IMAGING_DATA binary table.
 * @param imagingData structure where loaded information is stored.
 * @param errMsg error description message returned if function fails.
 *
 * @return
 * amdlibSUCCESS on successful completion. Otherwise amdlibFAILURE is returned.
 */
amdlibCOMPL_STAT amdlibReadImagingDataHdr
                                   (fitsfile           *filePtr, 
                                    amdlibIMAGING_DATA *imagingData, 
                                    amdlibERROR_MSG    errMsg)

{
    int         status = 0;
    char        fitsioMsg[256];

    amdlibLogTrace("amdlibReadImagingDataHdr()"); 

    /* Reset error message */
    memset(errMsg, '\0', sizeof(amdlibERROR_MSG));

    /* Clear 'IMAGING_DATA' data structure */
    memset(imagingData, '\0', sizeof(amdlibIMAGING_DATA));
           
    /* Go to the IMAGING_DATA binary table */
    if (fits_movnam_hdu(filePtr, BINARY_TBL, "IMAGING_DATA", 0, &status) != 0)
    {
        amdlibReturnFitsError("IMAGING_DATA");
    }

    /*** Read header informations */
    /* Read origine of the data and instrument name. If not found, the error
     * is ignored */
    if (fits_read_key(filePtr, TSTRING, "ORIGIN", &imagingData->origin,
                  NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "INSTRUME",
                      &imagingData->instrument, NULL, &status) != 0)
    {
        status = 0;
    }
    /* Read dates */
    if (fits_read_key(filePtr, TDOUBLE, "MJD-OBS", &imagingData->dateObsMJD,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "DATE-OBS", &imagingData->dateObs,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "DATE", &imagingData->date,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    /* Read dictionary names */
    if (fits_read_key(filePtr, TSTRING, "ESO DET DID", 
                      &imagingData->detDictionaryId, NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TSTRING, "ESO DET ID", &imagingData->detId,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    /* Read the number of regions */
    if (fits_read_key(filePtr, TINT, "NREGION", &imagingData->nbRegions,
                      NULL, &status) != 0)
    {
        amdlibReturnFitsError("NREGION");
    }   
    /* Read other parameters. If not found, the error is ignored */
    if (fits_read_key(filePtr, TINT, "MAXTEL", &imagingData->maxTel,
                      NULL, &status) != 0)
    {
        status = 0;
    }  
    if (fits_read_key(filePtr, TINT, "MAXINS", &imagingData->maxIns,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    if (fits_read_key(filePtr, TINT, "MAXSTEP", &imagingData->maxStep,
                      NULL, &status) != 0)
    {
        status = 0;
    }
    return amdlibSUCCESS;
}

/*___oOo___*/
