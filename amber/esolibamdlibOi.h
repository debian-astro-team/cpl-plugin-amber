/*
 * esolibamdlibOi.h
 *
 *  Created on: Oct 29, 2009
 *      Author: agabasch
 */

#ifndef ESOLIBAMDLIBOI_H_
#define ESOLIBAMDLIBOI_H_
#include "amdlib.h"
amdlibCOMPL_STAT amdlibSaveOiFile_waveselected(const char *filename,
		amdlibINS_CFG        *insCfg,
		amdlibOI_ARRAY       *array,
		amdlibOI_TARGET      *target,
		amdlibPHOTOMETRY     *photometry,
		amdlibVIS            *vis,
		amdlibVIS2           *vis2,
		amdlibVIS3           *vis3,
		amdlibWAVELENGTH     *wave,
		amdlibPISTON         *pst,
		amdlibSPECTRUM       *spectrum,
		amdlibERROR_MSG       errMsg,
		int                   lmin,
		int                   lmax);

#endif /* ESOLIBAMDLIBOI_H_ */
